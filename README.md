Linters
----------------------------------------------------------------

.eslintrc.json

- extends eslint:recommended
- intended for use with strictly ES5 code
- provides a base of rules for all eslint files

.eslintrc-es6.json

- extends .eslintrc.json
- adds support for ES6 rules

.eslintrc-react.json

- extends .eslintrc-es6.json
- adds support for React/JSX rules
- makes use of `babel-parser` and `eslint-react-plugin` for parsing and rules

index.js file contains examples of what will fail and reference links to ESHint documentation.

### Example usage in your package.json file
The following example will utilize the base eslintrc.json file

1. Add `code-linter` in the `devDependencies` in package.json
2. `npm install`
3. Resolve any unmet `peerDependencies` for this package by installing them manually
4. Add an `eslintConfig` section to package.json
5. Select a lint file to use and place in the `extends` property of the `eslintConfig` section


```json
{
  "devDependencies": {
    "code-linter": "git+ssh:git@bitbucket.org:caremanagment/code-linter.git#master"
  },
  "eslintConfig": {
    "extends": "./node_modules/code-linter/.eslintrc.json"
  }
}
```
